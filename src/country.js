$(document).ready(function() {
    'use strict';
    // VÙNG 1: Biến toàn cục 
    var gDatabase = []; // biến lưu all dữ liệu
    var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
    var gBaseUrl = "http://localhost:8080/countries";
    var gTable = $("#country-table").DataTable({
      "columns": [
        { "data" : "id" },
        { "data" : "countryCode" },
        { "data" : "countryName" },
        { "data": 'action' },
      ],
      columnDefs: [
        {
          targets: 3,
          defaultContent: `
            <div class="d-inline-flex">
              <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
              <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
            </div>
          `
        }
      ]
    })
  
    // VÙNG 2: gán sự kiện cho các phần tử
    $(document).ready(function() { // hàm chạy khi load trang
      callAjaxApiGetAllCountry();
    })
    $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
    gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
      onBtnUpdateDataClick(this)
    })
    gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
      onBtnDeleteDataClick(this)
    })
    $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
    $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
    $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
    
    // VÙNG 3: hàm xử lý sự kiện
    function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
      $("#modal-add-data").modal("show");
    }
    function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
      "use strict";
      $("#modal-update-data").modal("show");
      var vCurrentRow = $(paramButtonElement).closest("tr");
      var vRowData = gTable.row(vCurrentRow).data();
      gSelectedId = vRowData.id;
      console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
      loadDataToModalUpdateData(vRowData);
    }
    function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
      "use strict";
      $("#modal-delete-data").modal("show");
      var vCurrentRow = $(paramButtonElement).closest("tr");
      var vRowData = gTable.row(vCurrentRow).data();
      gSelectedId = vRowData.id;
      $("#span-data-id").html(gSelectedId);
      console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
    }
    function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
      gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
      // B1: đọc dữ liệu từ form
      var vCountryData = readDataFromModalAddData();
      // B2: validate dữ liệu
      var vIsValid = validateData(vCountryData);
      if (vIsValid == true) {
        // B3: gọi api
        callAjaxApiAddNewCountry(vCountryData);
        resetModalAddData();
        $("#modal-add-data").modal("hide");
      }
    }
    function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
      // B1: đọc dữ liệu từ modal update data
      var vCountryData = readDataFromModalUpdateData();
      // B2: validate dữ liệu
      var vIsValid = validateData(vCountryData);
      if(vIsValid == true) {
        // B3: gọi api
        callAjaxApiUpdateCountryById(gSelectedId, vCountryData);
        $("#modal-update-data").modal("hide")
      }
    }
    function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
      callAjaxApiDeleteCountryById(gSelectedId);
      $("#modal-delete-data").modal("hide")
    }
  
    // VÙNG 4: hàm dùng chung
    function callAjaxApiGetAllCountry() { // gọi api lấy list all dữ liệu
      $.ajax({
        url: gBaseUrl,
        type: "GET",
        async: false,
        dataType: "json",
        success: function(res) {
          console.log(res);
          gDatabase = res;
          loadListCountryToTable(gDatabase);
        },
        error: function(err) {
          console.log(err.response);
        }
      })
    }
    function loadListCountryToTable(paramListCountry) { // đổ list dữ liệu vào table
      "use strict";
      gTable.clear();
      gTable.rows.add(paramListCountry);
      gTable.draw();
    }
    function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
      var vCountryData = {
        countryCode: $("#input-countryCode-modal-add-data").val().trim(),
        countryName: $("#input-countryName-modal-add-data").val().trim(),
      };			
      console.log("Thông tin Country đọc được là:");
      console.log(vCountryData);
      return vCountryData
    }
    function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
      var vCountryData = {
        countryCode: $("#input-countryCode-modal-update-data").val().trim(),
        countryName: $("#input-countryName-modal-update-data").val().trim(),
      };			
      console.log("Thông tin Country đọc được là:");
      console.log(vCountryData);
      return vCountryData
    }
    function loadDataToModalUpdateData(paramCountry) { // đổ dữ liệu vào modal update data
      "use strict";
      $("#input-countryCode-modal-update-data").val(paramCountry.countryCode);
      $("#input-countryName-modal-update-data").val(paramCountry.countryName);
    }
    function validateData(paramCountryData) { // kiểm tra dữ liệu từ 2 modal
      "use strict";
      var vIsValid = false;
      if (paramCountryData.countryCode == "") {
        alert("Chưa nhập Mã Country")
      }
      else if (checkCountryCodeDaTonTai(paramCountryData.countryCode, gSelectedId)) {
        alert("Đã tồn tại Mã Country này");
      }
      else if (paramCountryData.countryName == "") {
        alert("Chưa nhập Tên Country")
      }
      else {
        vIsValid = true;
      }
      return vIsValid
    }
    function checkCountryCodeDaTonTai(paramCountryCode, paramId) {
      "use strict";
      // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
      var vIsExisted = false;
      var bI = 0;
      while (vIsExisted == false && bI < gDatabase.length) {
        if (gDatabase[bI].countryCode == paramCountryCode && gDatabase[bI].id != paramId) {
          // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng mã
          // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
          // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
          vIsExisted = true
        }
        else {
          bI ++
        }
      }
      return vIsExisted
    }
    function resetModalAddData() {
      $("#input-countryCode-modal-add-data").val("");
      $("#input-countryName-modal-add-data").val("");
    }
    function callAjaxApiAddNewCountry(paramCountryData) { // gọi api thêm dữ liệu
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl,
        type: "POST",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify(paramCountryData),
        success: function(res) {
          callAjaxApiGetAllCountry();
          console.log("Tạo Country mới thành công. Response là:");
          console.log(res);
          alert("Tạo Country mới thành công! Id mới là: " + res.id);
        },
        error: function(err){
          console.log(err.response);
          alert("Tạo Country mới không thành công! Xem console");
        }
      })
    }  
    function callAjaxApiUpdateCountryById(paramId, paramCountryData) { // gọi api cập nhật dữ liệu theo id
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl + "/" + paramId,
        type: "PUT",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify(paramCountryData),
        success: function(res) {
          callAjaxApiGetAllCountry();
          console.log("Update thành công cho Country có id " + paramId + ". Response là:");
          console.log(res);
          alert("Update thành công cho Country có id " + paramId);
        },
        error: function(err){
          console.log(err.response);
          alert("Update không thành công. Xem console");
        }
      })
    }
    function callAjaxApiDeleteCountryById(paramCountryId) { // gọi api xóa dữ liệu theo id
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl + "/" + paramCountryId,
        type: "DELETE",
        dataType: "json",
        success: function(res) {
          callAjaxApiGetAllCountry();
          console.log("Xóa Country có id " + paramCountryId + " thành công. Response là:");
          console.log(res);
          alert("Xóa Country có id " + paramCountryId + " thành công!");
        },
        error: function(err){
          console.log(err.response);
          alert("Xóa Country có id " + paramCountryId + " không thành công!");
        }
      })
    }
  })
  