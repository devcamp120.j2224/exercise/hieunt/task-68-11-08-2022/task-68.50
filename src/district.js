$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/districts";
  var gTable = $("#district-table").DataTable({
    "columns": [
      { "data" : "id" },
      {"data":"prefix"},
      {"data":"name"},
      {"data":"countryName"},
      {"data":"regionName"},
      { "data": 'action' },
    ],
    columnDefs: [
      {
        targets: 5,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllDistricts();
    callAjaxApiGetAllCountry();
  })
  $(".select-country").on("change", function() {  // hàm chạy khi thay đổi giá trị của select Country
    onCountrySelectChange(this)
  });
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onCountrySelectChange(paramCountrySelect) { // xử lý sự kiện thay đổi giá trị của select Country
    var vCountryId = $(paramCountrySelect).val();
    console.log("Đã chọn Country có id là: " + vCountryId);
    if(vCountryId == "") {
      $(".select-region").html("").append($("<option/>").val("").html("Chọn Region"));
    } else {
      callAjaxApiGetRegionsByCountryId(vCountryId);
    }
  }
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    resetModalAddData();
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    callAjaxApiGetRegionsByCountryId(vRowData.countryId)
    $("#modal-update-data").modal("show");
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vDistrictData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vDistrictData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewDistrict(vDistrictData);
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vDistrictData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vDistrictData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateDistrictById(gSelectedId, vDistrictData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteDistrictById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllDistricts() { // gọi api lấy list all dữ liệu District
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListDistrictToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListDistrictToTable(paramListDistrict) { // đổ list dữ liệu District vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListDistrict);
    gTable.draw();
  }
  function callAjaxApiGetAllCountry() { // gọi api lấy list all dữ liệu country
    $.ajax({
      url: "http://localhost:8080/countries",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadListCountryToSelect(res);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListCountryToSelect(paramListCountry) { // đổ list dữ liệu country vào các select
    "use strict";
    for (let bCountry of paramListCountry) {
      $(".select-country").each(function() {
        $(this).append($("<option>").val(bCountry.id).html(bCountry.countryName));
      })
    };
  }
  function callAjaxApiGetRegionsByCountryId(paramCountryId) {
    if(paramCountryId != "")
      $.ajax({
        url: "http://localhost:8080/regions/country/" + paramCountryId,
        type: "GET",
        async: false,
        dataType: "json",
        success: function(res) {
          loadListRegionSelect(res);
        },
        error: function(err) {
          console.log(err.response);
        }
      })
  }
  function loadListRegionSelect(paramListRegion) {
    "use strict";
    $(".select-region").html("").append($("<option/>").val("").html("Chọn Region"));
    for (let bRegion of paramListRegion) {
      $(".select-region").each(function() {
        $(this).append($("<option>").val(bRegion.id).html(bRegion.regionName));
      })
    };
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vDistrictData = {
      countryId: $("#select-country-modal-add-data").val().trim(),
      regionId: $("#select-region-modal-add-data").val().trim(),
      prefix: $("#select-district-prefix-modal-add-data").val().trim(),
      name: $("#input-district-name-modal-add-data").val().trim(),
    };			
    console.log("Thông tin District đọc được là:");
    console.log(vDistrictData);
    return vDistrictData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vDistrictData = {
      countryId: $("#select-country-modal-update-data").val().trim(),
      regionId: $("#select-region-modal-update-data").val().trim(),
      prefix: $("#select-district-prefix-modal-update-data").val().trim(),
      name: $("#input-district-name-modal-update-data").val().trim(),
    };			
    console.log("Thông tin District đọc được là:");
    console.log(vDistrictData);
    return vDistrictData
  }
  function loadDataToModalUpdateData(paramDistrict) { // đổ dữ liệu vào modal update data
    "use strict";
    $("#select-country-modal-update-data").val(paramDistrict.countryId),
    $("#select-region-modal-update-data").val(paramDistrict.regionId),
    $("#select-district-prefix-modal-update-data").val(paramDistrict.prefix);
    $("#input-district-name-modal-update-data").val(paramDistrict.name);
  }
  function validateData(paramDistrictData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramDistrictData.countryId == "") {
      alert("Chưa chọn Country")
    }
    else if (paramDistrictData.regionId == "") {
      alert("Chưa chọn Region")
    }
    else if (paramDistrictData.name == "") {
      alert("Chưa nhập Tên District")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function resetModalAddData() {
    $("#select-country-modal-add-data").val("");
    $("#select-region-modal-add-data").html("").append($("<option/>").val("").html("Chọn Region"));
    $("#select-district-prefix-modal-add-data").val("");
    $("#input-district-name-modal-add-data").val("");
  }
  function callAjaxApiAddNewDistrict(paramDistrictData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/region/" + paramDistrictData.regionId,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramDistrictData),
      success: function(res) {
        callAjaxApiGetAllDistricts();
        console.log("Tạo District mới thành công. Response là:");
        console.log(res);
        alert("Tạo District mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err);
        alert("Tạo District mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateDistrictById(paramId, paramDistrictData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramDistrictData),
      success: function(res) {
        callAjaxApiGetAllDistricts();
        console.log("Update thành công cho District có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho District có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteDistrictById(paramDistrictId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramDistrictId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllDistricts();
        console.log("Xóa District có id " + paramDistrictId + " thành công. Response là:");
        console.log(res);
        alert("Xóa District có id " + paramDistrictId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa District có id " + paramDistrictId + " không thành công!");
      }
    })
  }
})
