package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apicrud.backend.model.CRegion;

@Repository
public interface RegionRepository extends JpaRepository<CRegion, Long> {
	 /* CRegion findByRegionCode(String regionCode);
	 List<CRegion> findByRegionName(String regionName);	
	 List<CRegion> findByCountryId(Long countryId);
	 List<CRegion> findByCountryCountryCode(String countryCode);
	 List<CRegion> findByCountryCountryName(String countryName);
	 Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);	 */ 
}

