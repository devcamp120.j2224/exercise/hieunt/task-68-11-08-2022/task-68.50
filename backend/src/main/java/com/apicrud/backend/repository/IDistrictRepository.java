package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicrud.backend.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer>{
}
