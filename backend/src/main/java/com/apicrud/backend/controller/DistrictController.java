package com.apicrud.backend.controller;
 
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.apicrud.backend.model.*;
import com.apicrud.backend.repository.*;

@RequestMapping("/districts")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class DistrictController { 
    @Autowired
    IDistrictRepository iDistrictRepository;
    @Autowired
    RegionRepository regionRepository;

    @GetMapping("")
    public List<CDistrict> getAllDistricts() {
        return iDistrictRepository.findAll();
    }

    @GetMapping("/region/{regionId}")
    public ResponseEntity<Object> getDistrictsByRegionId(@PathVariable Long regionId) {
        Optional<CRegion> regionData = regionRepository.findById(regionId);
        if(regionData.isPresent()) 
            return new ResponseEntity<>(regionData.get().getDistricts(), HttpStatus.OK);
        else
            return new ResponseEntity<>("Region not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable int id) {
        Optional<CDistrict> districtData = iDistrictRepository.findById(id);
        if(districtData.isPresent())
            return new ResponseEntity<>(districtData.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
    }

    @PostMapping("/region/{regionId}")
    public ResponseEntity<Object> createDistrict(@PathVariable Long regionId, @RequestBody CDistrict pDistrict) {
        Optional<CRegion> regionData = regionRepository.findById(regionId);
        if(regionData.isPresent()) {
            try {
                CDistrict newDistrict = new CDistrict();
                newDistrict.setPrefix(pDistrict.getPrefix());
                newDistrict.setName(pDistrict.getName());
                newDistrict.setRegion(regionData.get());
                return new ResponseEntity<>(iDistrictRepository.save(newDistrict), HttpStatus.CREATED);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Create District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else
            return new ResponseEntity<>("Region not found", HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateDistrictById(@PathVariable int id, @RequestBody CDistrict pDistrict) {
        Optional<CDistrict> districtData = iDistrictRepository.findById(id);
        if(districtData.isPresent()) {
            try {
                CDistrict updatedDistrict = districtData.get();
                updatedDistrict.setPrefix(pDistrict.getPrefix());
                updatedDistrict.setName(pDistrict.getName());
                return new ResponseEntity<>(iDistrictRepository.save(updatedDistrict), HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Update District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int id) {
        Optional<CDistrict> districtData = iDistrictRepository.findById(id);
        if(districtData.isPresent()) {
            try {
                iDistrictRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch(Exception ex) {
                return new ResponseEntity<>("Failed to Delete District", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else
            return new ResponseEntity<>("District not found", HttpStatus.NOT_FOUND);
    }
}
