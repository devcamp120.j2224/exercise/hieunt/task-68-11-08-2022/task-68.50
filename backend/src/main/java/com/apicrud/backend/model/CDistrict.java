package com.apicrud.backend.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore; 

@Entity
@Table(name = "district")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "region_id")
    @JsonIgnore
    private CRegion region;

    @Transient
    private String countryId;
    @Transient
    private String countryName;
    @Transient
    private String regionId;
    @Transient
    private String regionName;

    public Long getCountryId() {
        return region.getCountry().getId();
    }
    public String getCountryName() {
        return region.getCountry().getCountryName();
    }
    public Long getRegionId() {
        return region.getId();
    }
    public String getRegionName() {
        return region.getRegionName();
    }

    public CDistrict() {
    }    
    public CDistrict(int id, String name, String prefix, CRegion region) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.region = region;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPrefix() {
        return prefix;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    public CRegion getRegion() {
        return region;
    }
    public void setRegion(CRegion region) {
        this.region = region;
    }       
}
